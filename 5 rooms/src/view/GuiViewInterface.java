package view;
/**
 * @author Aurora Laghi
 * The main menu.
 */
public interface GuiViewInterface {

    /**
     * starts the menu.
	 */
	public void go();
    /**
     * shows the menu
	 */
	public void show();
    /**
     * hides the menu
	 */
	public void hide();
}
