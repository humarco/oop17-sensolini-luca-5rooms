package game;

public interface Listener {
	/**
	 * notifies this listener
	 */
	public void notification();
}
